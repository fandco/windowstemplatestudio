﻿using System;
using System.Collections.ObjectModel;

using GalaSoft.MvvmLight;

using WindowsTemplateStudio.Models;
using WindowsTemplateStudio.Services;

namespace WindowsTemplateStudio.ViewModels
{
    public class blank_ChartViewModel : ViewModelBase
    {
        public blank_ChartViewModel()
        {
        }

        public ObservableCollection<DataPoint> Source
        {
            get
            {
                // TODO WTS: Replace this with your actual data
                return SampleDataService.GetChartSampleData();
            }
        }
    }
}
