﻿using System;

using CommonServiceLocator;

using GalaSoft.MvvmLight.Ioc;

using WindowsTemplateStudio.Services;
using WindowsTemplateStudio.Views;

namespace WindowsTemplateStudio.ViewModels
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            if (SimpleIoc.Default.IsRegistered<NavigationServiceEx>())
            {
                return;
            }

            SimpleIoc.Default.Register(() => new NavigationServiceEx());
            Register<PivotViewModel, PivotPage>();
            Register<MainViewModel, MainPage>();
            Register<blank_BlankViewModel, blank_BlankPage>();
            Register<blank_WebViewViewModel, blank_WebViewPage>();
            Register<blank_MediaPlayerViewModel, blank_MediaPlayerPage>();
            Register<blank_MasterDetailViewModel, blank_MasterDetailPage>();
            Register<blank_TelerikDataGridViewModel, blank_TelerikDataGridPage>();
            Register<blank_ChartViewModel, blank_ChartPage>();
            Register<blank_TabbedViewModel, blank_TabbedPage>();
            Register<blank_MapViewModel, blank_MapPage>();
            Register<blank_CameraViewModel, blank_CameraPage>();
            Register<blank_ImageGalleryViewModel, blank_ImageGalleryPage>();
            Register<blank_ImageGalleryDetailViewModel, blank_ImageGalleryDetailPage>();
            Register<SettingsViewModel, SettingsPage>();
            Register<UriSchemeExampleViewModel, UriSchemeExamplePage>();
            Register<ShareTargetViewModel, ShareTargetPage>();
        }

        public ShareTargetViewModel ShareTargetViewModel => ServiceLocator.Current.GetInstance<ShareTargetViewModel>();

        public UriSchemeExampleViewModel UriSchemeExampleViewModel => ServiceLocator.Current.GetInstance<UriSchemeExampleViewModel>();

        public SettingsViewModel SettingsViewModel => ServiceLocator.Current.GetInstance<SettingsViewModel>();

        public blank_ImageGalleryDetailViewModel blank_ImageGalleryDetailViewModel => ServiceLocator.Current.GetInstance<blank_ImageGalleryDetailViewModel>();

        public blank_ImageGalleryViewModel blank_ImageGalleryViewModel => ServiceLocator.Current.GetInstance<blank_ImageGalleryViewModel>();

        public blank_CameraViewModel blank_CameraViewModel => ServiceLocator.Current.GetInstance<blank_CameraViewModel>();

        public blank_MapViewModel blank_MapViewModel => ServiceLocator.Current.GetInstance<blank_MapViewModel>();

        public blank_TabbedViewModel blank_TabbedViewModel => ServiceLocator.Current.GetInstance<blank_TabbedViewModel>();

        public blank_ChartViewModel blank_ChartViewModel => ServiceLocator.Current.GetInstance<blank_ChartViewModel>();

        public blank_TelerikDataGridViewModel blank_TelerikDataGridViewModel => ServiceLocator.Current.GetInstance<blank_TelerikDataGridViewModel>();

        public blank_MasterDetailViewModel blank_MasterDetailViewModel => ServiceLocator.Current.GetInstance<blank_MasterDetailViewModel>();

        // A Guid is generated as a unique key for each instance as reusing the same VM instance in multiple MediaPlayerElement instances can cause playback errors
        public blank_MediaPlayerViewModel blank_MediaPlayerViewModel => ServiceLocator.Current.GetInstance<blank_MediaPlayerViewModel>(Guid.NewGuid().ToString());

        public blank_WebViewViewModel blank_WebViewViewModel => ServiceLocator.Current.GetInstance<blank_WebViewViewModel>();

        public blank_BlankViewModel blank_BlankViewModel => ServiceLocator.Current.GetInstance<blank_BlankViewModel>();

        public MainViewModel MainViewModel => ServiceLocator.Current.GetInstance<MainViewModel>();

        public PivotViewModel PivotViewModel => ServiceLocator.Current.GetInstance<PivotViewModel>();

        public NavigationServiceEx NavigationService => ServiceLocator.Current.GetInstance<NavigationServiceEx>();

        public void Register<VM, V>()
            where VM : class
        {
            SimpleIoc.Default.Register<VM>();

            NavigationService.Configure(typeof(VM).FullName, typeof(V));
        }
    }
}
