﻿using System;
using System.Collections.ObjectModel;

using GalaSoft.MvvmLight;

using WindowsTemplateStudio.Models;
using WindowsTemplateStudio.Services;

namespace WindowsTemplateStudio.ViewModels
{
    public class blank_TelerikDataGridViewModel : ViewModelBase
    {
        public ObservableCollection<SampleOrder> Source
        {
            get
            {
                // TODO WTS: Replace this with your actual data
                return SampleDataService.GetGridSampleData();
            }
        }
    }
}
