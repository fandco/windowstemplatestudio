﻿using System;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

using WindowsTemplateStudio.ViewModels;

namespace WindowsTemplateStudio.Views
{
    public sealed partial class blank_MapPage : Page
    {
        private blank_MapViewModel ViewModel
        {
            get { return DataContext as blank_MapViewModel; }
        }

        public blank_MapPage()
        {
            InitializeComponent();
            Loaded += blank_MapPage_Loaded;
            Unloaded += blank_MapPage_Unloaded;
        }

        private async void blank_MapPage_Loaded(object sender, RoutedEventArgs e)
        {
            await ViewModel.InitializeAsync(mapControl);
        }

        private void blank_MapPage_Unloaded(object sender, RoutedEventArgs e)
        {
            ViewModel.Cleanup();
        }
    }
}
