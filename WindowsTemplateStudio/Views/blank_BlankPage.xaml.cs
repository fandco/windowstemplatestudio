﻿using System;

using Windows.UI.Xaml.Controls;

using WindowsTemplateStudio.ViewModels;

namespace WindowsTemplateStudio.Views
{
    public sealed partial class blank_BlankPage : Page
    {
        private blank_BlankViewModel ViewModel
        {
            get { return DataContext as blank_BlankViewModel; }
        }

        public blank_BlankPage()
        {
            InitializeComponent();
        }
    }
}
