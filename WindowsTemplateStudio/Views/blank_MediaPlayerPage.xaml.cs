﻿using System;
using System.Linq;

using Microsoft.Toolkit.Uwp.UI.Extensions;

using Windows.Media.Playback;
using Windows.System.Display;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

using WindowsTemplateStudio.ViewModels;

namespace WindowsTemplateStudio.Views
{
    public sealed partial class blank_MediaPlayerPage : Page
    {
        private blank_MediaPlayerViewModel ViewModel
        {
            get { return DataContext as blank_MediaPlayerViewModel; }
        }

        // For more on the MediaPlayer and adjusting controls and behavior see https://docs.microsoft.com/en-us/windows/uwp/controls-and-patterns/media-playback
        // The DisplayRequest is used to stop the screen dimming while watching for extended periods
        private DisplayRequest _displayRequest = new DisplayRequest();
        private bool _isRequestActive = false;

        public blank_MediaPlayerPage()
        {
            Loaded += blank_MediaPlayerPage_Loaded;
            InitializeComponent();
        }

        private void blank_MediaPlayerPage_Loaded(object sender, RoutedEventArgs e)
        {
            var element = this as FrameworkElement;
            var pivotPage = element.FindAscendant<Pivot>();

            if (pivotPage != null)
            {
                pivotPage.SelectionChanged += PivotPage_SelectionChanged;
            }

            mpe.MediaPlayer.PlaybackSession.PlaybackStateChanged += PlaybackSession_PlaybackStateChanged;
            mpe.MediaPlayer.Play();
        }

        private void PivotPage_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bool navigatedTo = e.AddedItems.Cast<PivotItem>().Any(p => p.FindDescendant<blank_MediaPlayerPage>() != null);
            bool navigatedFrom = e.RemovedItems.Cast<PivotItem>().Any(p => p.FindDescendant<blank_MediaPlayerPage>() != null);

            if (navigatedTo)
            {
                mpe.MediaPlayer.PlaybackSession.PlaybackStateChanged += PlaybackSession_PlaybackStateChanged;
            }

            if (navigatedFrom)
            {
                mpe.MediaPlayer.Pause();
                mpe.MediaPlayer.PlaybackSession.PlaybackStateChanged -= PlaybackSession_PlaybackStateChanged;
            }
        }

        private async void PlaybackSession_PlaybackStateChanged(MediaPlaybackSession sender, object args)
        {
            if (sender is MediaPlaybackSession playbackSession && playbackSession.NaturalVideoHeight != 0)
            {
                if (playbackSession.PlaybackState == MediaPlaybackState.Playing)
                {
                    if (!_isRequestActive)
                    {
                        await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                        {
                            _displayRequest.RequestActive();
                            _isRequestActive = true;
                        });
                    }
                }
                else
                {
                    if (_isRequestActive)
                    {
                        await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                        {
                            _displayRequest.RequestRelease();
                            _isRequestActive = false;
                        });
                    }
                }
            }
        }
    }
}
