﻿using System;

using Windows.UI.Xaml.Controls;

using WindowsTemplateStudio.ViewModels;

namespace WindowsTemplateStudio.Views
{
    public sealed partial class blank_WebViewPage : Page
    {
        private blank_WebViewViewModel ViewModel
        {
            get { return DataContext as blank_WebViewViewModel; }
        }

        public blank_WebViewPage()
        {
            InitializeComponent();
            ViewModel.Initialize(webView);
        }
    }
}
