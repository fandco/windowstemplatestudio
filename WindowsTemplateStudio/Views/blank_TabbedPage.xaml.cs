﻿using System;

using Windows.UI.Xaml.Controls;

using WindowsTemplateStudio.ViewModels;

namespace WindowsTemplateStudio.Views
{
    public sealed partial class blank_TabbedPage : Page
    {
        private blank_TabbedViewModel ViewModel
        {
            get { return DataContext as blank_TabbedViewModel; }
        }

        public blank_TabbedPage()
        {
            InitializeComponent();
        }
    }
}
