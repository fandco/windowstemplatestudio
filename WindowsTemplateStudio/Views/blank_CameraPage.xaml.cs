﻿using Windows.UI.Xaml.Controls;

using WindowsTemplateStudio.ViewModels;

namespace WindowsTemplateStudio.Views
{
    public sealed partial class blank_CameraPage : Page
    {
        private blank_CameraViewModel ViewModel
        {
            get { return DataContext as blank_CameraViewModel; }
        }

        public blank_CameraPage()
        {
            InitializeComponent();
        }
    }
}
