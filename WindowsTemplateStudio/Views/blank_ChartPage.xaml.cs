﻿using System;

using Windows.UI.Xaml.Controls;

using WindowsTemplateStudio.ViewModels;

namespace WindowsTemplateStudio.Views
{
    public sealed partial class blank_ChartPage : Page
    {
        private blank_ChartViewModel ViewModel
        {
            get { return DataContext as blank_ChartViewModel; }
        }

        // TODO WTS: Change the chart as appropriate to your app.
        // For help see http://docs.telerik.com/windows-universal/controls/radchart/getting-started
        public blank_ChartPage()
        {
            InitializeComponent();
        }
    }
}
