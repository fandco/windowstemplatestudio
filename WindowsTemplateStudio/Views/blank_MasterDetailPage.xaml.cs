﻿using System;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

using WindowsTemplateStudio.ViewModels;

namespace WindowsTemplateStudio.Views
{
    public sealed partial class blank_MasterDetailPage : Page
    {
        private blank_MasterDetailViewModel ViewModel
        {
            get { return DataContext as blank_MasterDetailViewModel; }
        }

        public blank_MasterDetailPage()
        {
            InitializeComponent();
            Loaded += blank_MasterDetailPage_Loaded;
        }

        private async void blank_MasterDetailPage_Loaded(object sender, RoutedEventArgs e)
        {
            await ViewModel.LoadDataAsync(MasterDetailsViewControl.ViewState);
        }
    }
}
