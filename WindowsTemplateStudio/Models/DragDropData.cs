﻿using Windows.ApplicationModel.DataTransfer;

namespace WindowsTemplateStudio.Models
{
    public class DragDropData
    {
        public DataPackageOperation AcceptedOperation { get; set; }

        public DataPackageView DataView { get; set; }
    }
}
